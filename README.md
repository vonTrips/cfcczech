# College Football Coach - Career Edition (czech)
Jednoduchý veřejný repositář zaměřený na jednoduchou, ale skvělou mobilní hru **College Footbal Coach - Career Edition** (která se bohužel již nevyvíjí).

K dispozici je:
 * kompletně přeložený manuál (ne zcela aktuální)
 * osobní poznámky ke hře (primárně za slabší týmy)
 * aktuální rozdělení týmů a univerzit (adresář universe - bez reálných jmén trenérů a hráčů)
 * soupisky pro reálnější hru (adresář rooster - zatím chybí)
 * seznam možných vylepšení (níže)

## Seznam přání
Toto jsou věci, u kterých bych byl rád, kdyby byly ve hře implementovány:
 * Nove 12 místné playoff (5+7) + varianty s 16 týmy
 * Více Bowl games
 * Předělané konference/divize podle reality (některé konference mají rušit divize)
 * Lepší možnosti filtrování při rekrutování (chci např. inteligenci nad XY, nebo sílu)
 * Možnost tvorby shortlistu pro rekrutování
 * I cizí týmy mají rozpočet a vybírají si ze stejného množství hráčů (+ mají potřeby, které je třeba pokrýt => tzn. nemohou rekrutovat jen 5*)
 * Více lepších hráčů v náboru (chybí diverzita a dlouho trvá dostat se k lepším rekrutům)
 * Cena hráčů bude různá podle různých kritérií (blízkost, stejní hráči na pozici, reputace)
 * Každý tým si nejdříve vytvoří seznam hráčů, které chce, než si je "koupí" - je to kvůli efektivnímu využití peněz
 * Způsob náboru bude záviset na HC/AD, tedy jestli jsou ochotni vzít charakterově slabšího hráče, jestli koukají jen po hvězdičkách atd.
 * Překlady hry
 * Aktualizovat a vylepšit návod (např. jaká taktika je vhodná jako proti konkrétní ofenzivě; jaký typ hráče je důležité rekrutovat atd.)
 * I u návodu umožnit rekrutování a ideálně aby byl návod součástí hry (**moc netuším co to mělo být**)
 * Opravit ukládání před rekrutováním (popř. doplnit i o uložení uprostřed sezóny)
 * Prohození hráčů CB/S a DL/LB, popř. RB/TE/WR v případě, že někde je více dobrých hráčů
 * Přesvědčení hráčů, aby nešli na draft?
 * CUT hráčů je možné provést jen před transfer portalem, popř. na začátku sezóny pokud má tým víc než minimální počet hráčů (celkem 61 + pozice, ale asi se to zvýší na 65 minimum a 85 maximum)
 * CUT hráči jsou pak ti, kteří se také zapojí do transfer portalu
 * Počet nových hráčů bude omezen na 25 za rok, tzn. že CUT může být na maximálně 40 hráčů (40 stávajících + 25 nových)
 * Vylepšený transfer portal (větší výběr hráčů, hráč nemusí rok sedět atd.), aby to nebyl jen jeden nebo dva náhodně vybraní hráči
 * U odchozích hráčů možnost je zkusit přemluvit, aby zůstali místo transferu?
 * Možnost vyhodit OC/DC po sezóně, stejně jako je CUT hráče
 * Návod k facilities a TV právům
 * Přidat rivality + rivality game
 * Custom Start Date (možnost vrátit se se starším CSV souborem do minulosti, např. roku 2015; nebo naopak se posunout dál)
 * Vysvětlivky při nastavování nové hry (není úplně jasné, co která položka znamená a manuál není aktuální)
 * V roosteru by měl být uveden u každé pozice minimální počet hráčů + počet starterů
 * Možnost nastavit pořadí hráčů na Depth Chart, aby se nebral vždy ten lepší (např. chci aby hrál FR 67, než SR 70)
 * U Depth Chart zvýraznit/seřadit hráče podle potřebné vlastnosti (např. u Option chci mobilního QB, takže mít možnost zvýraznit, popř. seřadit hráče podle Evasion/Speed)
 * Je třeba určit jak velká je rotace hráčů pro zápasy (např. závislé na energii?)
 * Specifikovat cestu pro uložení/export hry kvůli snadnější synchronizaci mezi zařízeními
 * Popř. vymyslet nějakou chytrou a jednoduchou variantu synchronizace mezi zařízeními
 * Při uložení hry uložit i depth chart a schedule
 * Možnost odlákat HC/OC/DC z jiných týmů (platí oboustranně)
 * Přidat národy, vlajky a obličeje (např. faces.js) - minoritně dát i jiné národnosti než US
 * Možnost jednou za čas přidat novou univerzitu (prostě někdo postoupí do D1-FBS)
 * Možnosti přestupů/pádů mezi FBS/FCS/D2
 * Možnost návratu bývalých hráčů (nejen na Alma Mater) do pozic HC/DC/OC popř. i dalších
 * Konferenční prestiž se nepočítá jen jako průměr týmů, ale započítávají se i bonusy za bowly a výsleky
 * Více ofenzivních i defenzivních strategií (např. 3-4 obrana, Nickel, 3-3-5 atd.)
 * Měnit počet startujících hráčů na každé pozici podle zvoleného schématu 
 * V závislosti na schématu zvýraznit důležité vlastnosti u hráčů na daných pozicicích (např. pro Smash Mouth je to Run Block pro OL atd.)
 * Vytvořit Squad Planner (obdoba z FM)
 * Možnost filtrování osob při najímání HC/DC/OC
 * Prestiž týmu se změní i na základě draftovaných, All-American a All-Pro hráčů
 * Universe data se loadují z JSON (oby bylo možné využít datové soubory z College Dynasty)
 * Doplnit popisky hráčů a jejich dělení podle Football Coach 2 (lépe rozdělit/rozlišit jejich vlastnosti)
 * Určitě více zapracovat zkušenosti a inteligenci
 * Před sezónou mít tréninky, aby bylo možné v omezené míře (např. u 10 % hráčů) zlepšit některé atributy hráčů, jako je to možné u CFC Dynasty
 * RedShirt hráči zlepšují pouze své fyzické dovednosti (síla, rychlost, obratnost, popř. inteligenci = lepší znalost playbooku); skills se zlepšují pouze hrou nebo jarními tréninky
 * Značku RS ukázat u každého, kdo už absolvoval sezónu jako RedShirt
 * Číselné atributy i u známých hráčů budou nahrazeny za známky A až F (popř. A+, B+ atd.)
 * Potenciál zobrazovat jako hvězdičky (ale nemusí být reálný, viz skryté diamanty), stejně jako skautované vlastnosti před náborem
 * Hvězdy se určí podle kvality skauta + trochu náhodně (např. při horším skautingu bude hodnota 95 jako B) - *každý tým "uvidí" stejné hvězdy?*
 * U každého (i rekrutovaného) hráče mít možnost uložit si poznámku, např. jen 255 znaků

Nějak zkusit vymyslet, jak do rozpočtu zakomponovat NIL