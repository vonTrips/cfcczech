# Football Coach 2 (Android)
Originální hra, je trošku propracovanější na playcalling a staty hráčů. Ale rekrutování je trošku na hovno, protože není jistota, že daní hráči přijdou k Vám.

A tady jsou nějaké strategie:
* https://www.reddit.com/r/FootballCoach/comments/8e8xds/tips_to_play_better_coach_edition/
* https://www.reddit.com/r/FootballCoach/comments/7z1zwe/cfc_help/
* https://www.reddit.com/r/FootballCoach/comments/9h0fyy/which_playbook/
* https://www.reddit.com/r/FootballCoach/comments/9tli1b/college_football_coach_career_recruiting/
* https://www.reddit.com/r/FootballCoach/comments/bd8mts/best_offensive_and_defensive_philosophies/

Ideální filozofie se zdá být Spread (Air Raid)

## Spread offense (Air Raid)
Tady je to jednoduché:
* špičkový QB s dokonalou přesností (raději druhý mladý v záloze)
* špičkoví WR (rychlost, uvolnění i chytání)
* průměrná OL (hlavní je pass blok, run může být slabý)
* obyčejný RB (spíše pasový, ale bude hlavně do počtu)

Vždy je lepší vsadit na hráče s potenciále A+/A a vysokou mírou durability (nejhůře B). Mnohdy je A+ 3* lepší než C 4* hráč.
Cílem rekrutování je vzít vždy jednoho dobrého hráče na danou pozici (vyjma RB, QB a K) každý rok
U obrany vsadit na univerzálnost (možná jít cestou man coverege???)

## Ideální volba strategií
Volba proti obraně:
46 bear     <<< spread, west coas, read option
cover 0     <<< spread (dobrý QB), smash mouth (zbytek)
43 man      <<< spread, read option, smash mouth (dobří RB a OL)
cover 2/3   <<< smash mouth, read option

Volba proti útoku
smash mouth     <<< 46 bear
spread          <<< cover 3
read option     <<< cover 2
west coast      <<< cover 2
pro style       <<< 43 man

Obecne 46 proti běhu, a Cover 2/3 proti pasu

## Archetypy hráčů
* **A great athlete and a great student ...** -> vše v rozsahu A/C, takže spíše jako nějaký backup
* **A real gym rat ...** -> A+ potencial, zbytek různé (může se jednat o ideální startery)
* Came from a very bad neighborhood ... -> žádná nápověda, dává jakékoli kombinace
* Concerns about his love of game ... -> F+ potencial, ale zpravidla dobrá durability (pokud není 5* tak se moc nehodí)
* Didn't come to play school -> těšný souboj nevyhraješ, ale když ostatní přeplatíš...
* **Didn't play footbal until he was 15 ...** -> A potencial, zbytek různé (ideální starteři)
* **Doesn't like to talk about practice** -> slušný potenciál (A/C), ale horší IQ (C/D)
* Forgot X plays ... -> může značit slabší durability, ale určitě značí slabý potenciál (C+ nejlépe)
* *Grew up the son of an NFL player ...* -> spíše jen průměrný potenciál (dobrý backup)
* Has a sweet tooth ... -> nižší potenciál (C/D) a IQ (B/C)
* *Has a burger named ...* -> pravděpodobně stejný potenciál, durability i IQ (může jít o risk)
* Has durability concerns ... -> F+ durability, potenciál jen výjimečně A, spíše C/D (nehodí se)
* Has had X surgeries ... -> F durability, potenciál jen výjimečně A (nehodí se)
* **Has incredible physical gifts ...** -> A+ potencial a slušná durability (ideální starter)
* **Has nerves of steel, good at making clutch ...** -> slušný potenciál i durability (nemusí být v kombinaci)
* He is all business on the field ... -> zpravidla horší potenciál
* *Is known to play FC2...* -> slabší potenciál (C) i IQ (C)
* Is prolific on social media ... -> asi jdou primárně pro prestiži, při různých kvalitách; spíše nebrat
* Known as 'Mr Glass' ... -> durability D/F (určitě nebrat)
* Known to be a joker ... -> skryje cokoli, jen IQ bývá nižší (B max)
* *Known to make bone mistakes ...* -> zpravidla dobrý potenciál (B), ale špatné IQ (D/F+)
* Lost ten pounds ... -> čím méně hvězd, tím horší hodnocení, jen výjimečně A potenciál, ostatní nejlépe B
* **Never missed a game** -> A+ Durability, zbytek různý - lze vzít jako backup
* Never much of a weight room guy -> nulový potenciál (F) a jdou po prestiži (takže přijdou i při 0% šanci!!!)
* **Nicknamed 'Wolverine' ...** -> A+ durability, ale jakýkoli potenciál
* Nothing particularly interesting ... -> BBC
* **Once said he would die ...** -> A durability, ale jakýkoli potenciál
* One time missed a playoff game... -> horší potenciál (A/B jen výjimečně) a slabší IQ (C/F)
* *Perfectly fits the dumb jock stereotype* -> IQ jen F+, jinak může být OK (nehodí se pro QB a S, u OL/DL příliš nevadí)
* Planted his school's flag ... -> asi známka různých potenciálů, ale slabého IQ
* **Shows tremendous flash ...** -> zpravidla solidní potenicál (A/B) a různé durability + IQ
* *Spends more time browsing reddit ...* -> slušný potenciál (B/C), ale horší IQ (C/D)
* Started on his varsity team as freshman -> netuším k čemu to je, potenciál je různý A-D
* Starred in a hit dunk highlight video... -> spíše (horší) průměr, ale schovat může cokoli
* Was also a 5-star basketball prospect... -> průměrný hráč, který měl zůstat u basketu (C potenciál + IQ)
* Was a late bloomer in high school ... -> průměrný hráč s horším IQ, ale může mít A potenciál (spíše jako backup)
* Wants to go to a school close to Chik-Fil-A -> průměrný hráč, může mít horší IQ

## Další poznámky a poznatky
* Hráči odcházejí (gradují) na základě věků, ale i výsledků a jejich kvality (JR nad 90 po vítězném šampionátu odejde určitě)
* Football IQ je důležité u QB (5x), skill pozicím se hodí taky, ale OL má jen 1/5
* Football IQ je důležité pro S (5x), pro CB je vhodné, u DL/LB jen 1/7
* Football IQ je důležité pro výhodu domácího prostředí, resp. pro její vymazání venku; počítá se jen dle starterů
* Do PassBlock se započítává rating QB (logické, lajna jen pomáhá on nesmí být dřevák)
* Do RunBlock se započítává krom lajny jen RB
* pro PassBlock i RunBlock je důležitá síla stejně jako schopnost blokovat
* pro RunBlock jsou důležitější DL (2x)