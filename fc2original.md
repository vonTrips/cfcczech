# Football Coach 2 - překlad originálu
## Základy
Ve hře Football Coach se ujmete role nově přijatého trenéra na vybrané vysoké škole. Jako trenér budete mít za úkol řídit strategii svého týmu, simulovat v průběhu sezóny, nabírat nové hráče a především vyhrávat šampionáty.

## Sezóna
Váš tým odehraje v základní části 12 zápasů, které se skládají z jednoho zápasu se všemi 9 soupeři v konferenci a tří zápasů mimo konferenci. Když přejdete na záložku "GAMES", můžete přesně vidět, jaký je rozpis zápasů vašeho týmu. A po kliknutí na každý zápas si můžete prohlédnout skautskou zprávu obou týmů (pokud zápas ještě nebyl odehrán) nebo shrnutí zápasu (pokud již byl odehrán). Nejdůležitějším zápasem základní části je "rivality game", za který v případě výhry získáte prestiž, v případě prohry prestiž ztratíte. 

Na konci základní části se hraje finále konference mezi dvěma týmy s nejlepší bilancí v konferenci (vítězové obou divizí). Pokud mají dva týmy stejný rekord, rozhoduje vzájemný souboj. Pokud mají tři nebo více týmů stejnou bilanci, rozhoduje tým s nejvyšším umístěním v anketě. Po skončení finále konferencí se hrají zápasy Bowl Games. Do bowlu je vybráno pouze 16 nejlepších týmů, přičemž nejdůležitější je národní semifinále mezi 1. a 4. týmem a 2. a 3. týmem. Vítěz těchto dvou zápasů postoupí do národního šampionátu, kde je korunován nezpochybnitelný šampion. 

Po skončení sezóny váš tým získá nebo ztratí prestiž na základě svých výsledků oproti očekáváním a vy přejdete k náboru.

## Ranking
Jak váš tým hraje v průběhu sezóny, hodnotí ho tazatelé, kteří určují, jak dobrý (nebo špatný) je váš tým v porovnání s ostatními. Toto "pořadí v anketě" se určuje podle výher, rozdílu vítězství, síly rozpisu (SOS) a dalších faktorů a má velký vliv na to, jak bude vaše sezóna probíhat, protože rozhoduje o tom, kdo se dostane do bowlu a semifinále. V průběhu sezóny se sleduje mnoho statistik, například Yards Per Game, Opposing Points Per Game, Pass Yards Per Game, Turnover Differential a další. Všechny tyto statistiky, stejně jako pořadí vašeho týmu v jednotlivých statistikách, si můžete prohlédnout, když přejdete na kartu "STATS".

## Soupiska (Rooster)
Když tým zdědíte poprvé, budete mít na soupisce celkem ~~46 hráčů (22 hráčů základní sestavy)~~ toto neplatí, hráčů je více. U každého hráče je uvedeno jeho jméno, školní rok (od prváka po seniora), celkové hodnocení, hodnocení potenciálu a tři hodnocení specifická pro jednotlivé pozice. Celkové hodnocení je pouze složeným hodnocením tří hodnocení specifických pro danou pozici, zatímco potenciální hodnocení určuje, jak moc se který hráč během sezóny zlepší. Všechny tyto hráče si můžete prohlédnout na kartě "ROSTER".

Starterů na obou stranách míče je 12, ale personel se volí podle konkrétní hry. Například I-form využije TE, 2 RB a jen 2 WR.

### QB (1 starter)
* Zodpovědný za passy na WR
* Pass Strength - jak daleko QB dohodí
* Accuracy - jak přesně QB háže (vyšší = vyšší šance na catch a nižší šance na Int)
* Evasion - jak je QB pohyblivý (vyšší hodnota = méně sacků)
* FootballIQ - důležité pro snížení/zvýšení výhody prostředí a hlavně snížení šance na Int

### RB (2 starters)
* RB běhají s míčem
* Rush Power - jak silný RB je, čím lepší, tím hůř se zastavuje
* Rush Speed - jak rychlý je (vyšší = více yardů)
* Evasion - vyšší = větší šance na dlouhé běhy

### WR (3 starters)
* WR jsou ti, kteří zachytávají přihrávky od QB
* Catching - čím vyšší hodnota, tím méně dropů
* Speed - čím vyšší hodnota, tím delší routy může běhat (= potenciální zisk více yardů)
* Evasion - schopnost se uvolnit = YAC

### OL (5 starters)
* Lajna je zodpovědná za ochranu QB
* Strength - nejdůležitější rating, OL musí být silný
* Run Block - jak dobře OL blokuje pro běh (irelevantní pro Spread)
* Pass Block - jak dobře OL blokuje pro pass (irelevantní pro Smashmouth)

### K (1 starter)
* Kicker se stará o všechny kopy ve hře
* Kick Strength - jak daleko je schopný kopnout
* Kick Accuracy - jak přesně je schopen kopat
* Clumsiness - šikovnost je důležitá hlavně pro onside kick

### DL (4 starters)
* popisek v originálu chybí
* ale DL hlavně vytvářejí tlak na QB
* Strength - nejdůležitější parametr u DL (musí přetlačit OL)
* Pass pressure - jak hodne dokáží presovat QB (vyšší hodnota znamená méně času pro QB)
* Run Stop - jak dobře dokáže zastavit (tacklovat) RB

### LB (3 starters)
* popisek v originálu chybí
* ale LB jsou hlavně zodpovědní za zastavování běhu (tedy RB)
* Strength - nejsem si jist, že i u LB je to nejdůležitější parametr
* Pass pressure - jak hodne dokáží presovat QB (vyšší hodnota znamená méně času pro QB)
* Run Stop - jak dobře dokáže zastavit (tacklovat) RB

### S (2 starter)
* Safety je zopdovědný za coverege při pasové hře a má největší šanci na interception
* Coverege - jak dobře pokrývá pasovou hru (vyšší hodnota = vyšší šance na int)
* Speed - jak je rychlý, tedy jak rychle se dostane/pokryje WR
* Tackling - vyšší hodnota = vyšší pravděpodobnost skládky a tedy zastavení hráče s míčem

### CB (3 starters)
* CB pokrývají WR nejen v osobní obraně
* Coverege - jak dobře porkývá WR (vyšší hodnota = nižší šance na catch pro WR)
* Speed - jak je rychlý, tedy jestli pokryje WR, popř. jej předběhne
* Tackling - vyšší hodnota = vyšší pravděpodobnost skládky a tedy zastavení hráče s míčem

## Týmové strategie
tady je oproti poslednímu manuálu tolik změn, že to ani nepřepisuju

## Rekrutování
Na konci každé sezóny opouštějí program senioři a uvolňují se místa. Jako trenér jste zodpovědní za nábor dalšího ročníku hráčů, kteří dovedou váš tým k větším a lepším vítězstvím. Nábor probíhá na základě rozpočtu, který je určen prestiží vašeho týmu. Lepší týmy budou mít k dispozici více peněz (bodů), zatímco horší týmy budou muset šetřit, kde se dá.

Když po sezóně stisknete tlačítko "Begin Recruiting", uvidíte, kdo váš program opouští, a budete mít přehled o tom, kolik hráčů budete potřebovat nahradit. Dále se otevře nabídka Nábor. Můžete si zobrazit nábory z každé pozice nebo vybrat "Top 100 Recruits" a zobrazit si ty nejlepší z nejlepších. U každého rekruta je uvedeno jeho hodnocení podle pozice a také celkové hodnocení a hodnocení potenciálu. Cena každého rekruta (zde vložte vtip o Camu Newtonovi) se určuje podle toho, jak dobrý je. Jakmile dokončíte nábor všech hráčů, které potřebujete nebo na které máte peníze, můžete stisknout tlačítko "Hotovo" a postoupit do další sezóny.