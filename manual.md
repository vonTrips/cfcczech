Překlad a doplnění manuálu: https://www.antdroid.dev/p/game-manual.html

Základem je jednoduchost s možností si vše nastavit, upravit a připravit pro vlastní zájem. Vždy se hraje 12 zápasů v sezóně (nevím jestli existuje rivality game) - 9 v rámci konference a 3 mimokonferenční. Následuje konferenční finále a poté bowly, kdy SEED 1 až 4 hrají semifinále a vítězové pak Národní finále (NC).

# Navigace
 * [Home screen](#home-screen)
 * [Game Options](#game-options)
 * [Main Screen](#main-screen)
 * [Game Menu](#game-menu)
 * [Player Position Info](#player-position-info)
 * [Player & Coach Progression](#player-coach-progression)
 * [Offense & Defense Strategies](#offense-defense-strategies)
 * [Recruiting](#recruiting)
 * [Editor](#game-editor)


# Home screen

Po vstupu do hry se jako první zobrazí domovská obrazovka. Bude zde několik tlačítek, na která můžete kliknout a provést tak různé funkce při nastavování hry.

## New Game

Tímto tlačítkem spustíte novou hru s použitím výchozí databáze. Každý tým ve hře je fiktivní, ale může být založen na skutečném týmu. Hráči a trenéři jsou generováni pomocí databáze 15 000 jmen.

## Custom Game

Tímto tlačítkem spustíte vlastní hru s použitím externích databází. Tyto databáze jsou soubory TXT nebo CSV pro univerzity, soupisky a trenéry.

**Custom DB download**: https://drive.google.com/drive/folders/1hfB_lbTaMfhm4lXtMelvgWoZSshL12v0

**Custom DB návod**: https://www.antdroid.dev/p/custom-data.html

### Návod na vložení
1. Klikněte na položku Vlastní hra
2. Na výzvu stiskněte tlačítko OK
3. Vyberte soubor Universe2018v2.txt
4. Vyberte libovolnou možnost (výchozí)
5. Importovat vyberte Ano
6. Zvolte Importovat seznam
7. Vyberte Roster2018-v4.txt
8. Zvolte Importovat další data (ano)
9. Zvolte Importovat soubor trenéra
10. Zvolte Coach2018.txt
11. Klikněte na tlačítko Ne pro import dalších dat
12. Vyberte možnosti hry a pokračujte ve hře

## Load Game

Tímto tlačítkem se načte uložená hra. K dispozici je 10 slotů pro uložení. Obrazovka Načíst je také dobrým nástrojem pro přehled o vaší trenérské kariéře a pro porovnání s ostatními uloženými hrami ve vašem zařízení. Upozornění: Každá načtená hra začne na začátku sezóny, ve které uživatel skončil. Uložení uprostřed sezóny prozatím není povoleno.

## Import Game

Jedná se o novou funkci, která uživatelům umožňuje importovat soubory Save Files exportované pomocí funkce Export ve hře. To se hodí při přenosu uložených souborů mezi zařízeními nebo při úpravě uloženého souboru pomocí externího textového editoru.


# Game options

Po spuštění nové hry se zobrazí řada výzev, které vám pomohou zaměřit se na nový vesmír. První z nich je databáze herní prestiže:
 * **Default** - výchozí nastavení, které je blízko realitě v době vydání hry;
 * **Random** - zcela náhodné nastavení týmů (konference by měly sedět), zvláště s ohledem na prestiž; silné konference zůstanou silnými
 * **Equalize** - všichni začínají na stejné startovní čáře
 
## Nastavení hry
 * **Show Player Potential** - Ve výchozím nastavení je tato funkce vypnutá, což znamená, že potenciální hodnocení každého hráče je skryté, aby hra nebyla tak předvídatelná. Pokud je tato možnost zapnutá, zobrazí se v profilu hráče dvě celková hodnocení. První je jeho aktuální Celkové hodnocení a druhé je jeho potenciální Celkové hodnocení v budoucnu. Toto budoucí hodnocení je velmi závislé na mnoha faktorech, včetně dostatku herního času, absence zranění a konzistentní nadprůměrné hry.
 * **Show Full Game Play by Play** - Hra zaznamenává záznamy o všech hrách odehraných v průběhu sezóny, včetně všech zápasů uživatele proti CPU a CPU proti CPU. Ve výchozím nastavení se v záznamu zobrazují pouze skórující hry a důležité momenty hry. Pokud je tato možnost povolena, je každá hra zaznamenána a uložena pro pozdější zobrazení. To však zpomaluje dobu simulace a může to mít značný vliv na starší zařízení.
 * **Career Mode (Fireable)** - Pokud tuto funkci povolíte, bude moci být uživatel propuštěn z práce stejně jako ve skutečném životě. Pokud budete mít několik špatných sezón, můžete být vyhozeni ještě před vypršením smlouvy, nebo vám po jejím vypršení nebude prodloužena. Pokud tato možnost není zaškrtnuta, je propouštění trenérů USER zakázáno. Trenéři CPU budou nadále propouštěni a najímáni.
 * **Fountain of Youth Mode** - Jakmile dosáhnete 70 let, budete ve výchozím nastavení vyzváni, abyste odešli do důchodu, převtělili se do nové osoby ve stejné hře nebo pokračovali ve hře se zapnutými efekty stárnutí. Pokud povolíte režim Fontána mládí, nebudete nikdy vyzváni k odchodu do důchodu a po 70. roce nebudete mít žádné efekty stárnutí.
 * **Expanded Playoffs** - dojde ke zrušení Bowlů a playoff bude 16místné. 10 vítězů konferencí a 6 zbylých nejlépe nasazených týmů.
 * **Enable TV Contracts** - Díky této možnosti se do hry dostává trochu obchodního světa. Televizní sítě každoročně uzavírají smlouvy s konferencemi a školám v rámci těchto konferencí zvyšují prestiž každou sezónu. To může pomoci náboru mimo sezónu a zajistit lepší mediální pokrytí, které může ovlivnit průzkumy veřejného mínění.
 * **Enable Conf Realignment** - Povolení této funkce umožňuje konferencím vyměnit si týmy jednou za několik let, pokud je splněno několik kritérií mezi oběma konferencemi a příslušnými týmy. Nestává se to často, ale může se to stát, stejně jako ve skutečném životě.
 * **Enable Advanced Conf Realignment** - Jedná se o šílenou verzi předchozí možnosti. Aby tato možnost fungovala, musí být povoleny obě možnosti. To umožňuje konferencím přebírat týmy od sebe navzájem, přebírat nezávislé týmy a spoustu dalších možností.
 * **Enable Promotion/Relegation** - soccer verze s postupy a sestupy; ligy po 10 týmech, první 2 automatický postup, poslední 2 automatický sestup.
 
## Další nastavení
Po potvrzení předchozích voleb následuje:
 * **Volba týmu** - dle vlastních preferencí 
 * **Volba jména** - nastavení vlastního jména 
 * **Nastavení vlastního stylu** - vyrovnaný, defenzivní nebo ofenzivní (u toho asistenta netuším, co to znamená) 
 * **Volba ofenzivního schéma** - uživatelským manažerem preferované ofenzivní schéma (viz níže) 
 * **Volba defenzivního schéma** - uživatelským manažerem preferované devenzivní schéma (viz níže) 
 
## Season Goals
Na začátku každé sezóny budete vyzváni k zadání cílů pro danou sezónu. Zvláštní pozornost věnujte předpovídanému pořadí. Pořadí bude nejvíce rozhodovat při výpočtu vašeho celkového skóre v sezóně a změny prestiže. Na bilanci W-L záleží jen nepatrně, protože síla vítězství je větším určujícím faktorem pro vaše celkové skóre sezóny.

Tato výzva vám také umožní uložit hru. Každé uložení bude začínat na začátku sezóny.


# Main Screen

Hlavní obrazovka hry má různá tlačítka v horní a dolní části obrazovky a uprostřed se zobrazuje obsah. Shora dolů:

V horní části stránky se zobrazí rozbalovací nabídka konference a týmu. Pomocí ní můžete vybrat a zobrazit ostatní týmy v lize. Tlačítko Domů v pravém horním rohu vás přenese zpět do vašeho uživatelského týmu.

Na dalším řádku se zobrazí název aktuálně vybraného týmu, jeho pořadí a rekord.

Na třetím řádku dole bude řada tlačítek, která zobrazí různé stránky. Každé tlačítko bude popsáno níže:

* **Stats** - Na stránce Statistiky se zobrazí obecný přehled toho, jak si vybraný tým vede v průběhu sezóny v porovnání se zbytkem ligy.
* **Stats** - Na této stránce se zobrazí soupiska vašeho týmu. Kliknutím na konkrétního hráče se zobrazí jeho atributy a statistiky a také statistiky jeho kariéry.
* **Stats** - Na této stránce najdete rozpis zápasů týmu.

Kliknutím na středové pole před odehraným zápasem se zobrazí náhled utkání pro daný zápas. Uživatelé mohou v tomto rámečku také upravovat herní strategie.

Po odehrání zápasu mohou uživatelé kliknout na políčko zápasu a zobrazí se souhrn zápasu a podrobné informace o skóre v boxu a záznamu hry.

* **Scores** - Zobrazuje tabulku výsledků v celé lize.
* **Standings** - Na této stránce najdete aktuální pořadí v celé lize.
* **Rankings** - Na této stránce se zobrazuje aktuální žebříček ligy.

Na spodní části jsou pak tlačítka
* **Redshirt** - Toto tlačítko se zobrazuje v předsezóně. Uživatelé mohou vybrat až 7 hráčů, kteří mají být přeřazeni. Každé redshirt může vyžadovat, aby hráč na soupisce obsadil pozici walk-on. Walk-ons se započítávají do soupisky týmu. Každá soupiska je omezena na 70 hráčů.
* **Depth Chart** - Po zahájení sezóny se tlačítko Redshirt změní na tlačítko Deptch Chart. Tato oblast umožňuje uživatelům nastavit startovní pozice pro každou pozici. Po nastavení každé pozice nezapomeňte kliknout na tlačítko ULOŽIT.
* **Playbook** - Toto tlačítko zobrazí herní strategie, které má tým k dispozici.
* **SIM** - Tímto tlačítkem se hra posune do následujícího týdne a simulují se všechny aktivity týdne.


# Game Menu

Herní nabídka se nachází v pravém horním rohu herní obrazovky. Tato rozbalovací nabídka obsahuje řadu položek od nastavení přes statistiky až po historické údaje. Níže je uveden stručný přehled jednotlivých položek nabídky:
* **Settings/Editors** - V této části můžete aktualizovat nastavení a úpravy hry. V této sekci je také k dispozici herní editor. Editor umožňuje uživatelům upravovat názvy týmů, názvy konferencí, jména trenérů a další. Některá nastavení mohou být dostupná pouze v určitých obdobích sezóny.
* **League History/Records** - Podle názvu se otevře okno, ve kterém si uživatelé mohou prohlédnout přehledy předchozích lig, ligové rekordy, historické statistiky a Síň slávy.
* **Poll History** - V této nabídce se zobrazují žebříčky Top 25 z každé předchozí sezóny.
* **Team History** - Tato položka nabídky otevře historii, statistiky, síň slávy atd. aktuálně vybraného týmu.
* **Coach Database** - Tato položka nabídky otevře databázi všech trenérů, kteří byli přítomni v uložené hře. Uživatelé si mohou zobrazit pořadí a kliknutím na jednotlivé trenéry otevřít jejich profily.
* **Save League** - Uživatelé si tady mohou uložit svá herní data, pokud tak neučinili během předsezónní přípravy. Upozorňujeme, že ukládací soubory ukládají pouze data z předsezóny a neukládají postup během sezóny.
* **Export League** - Tato možnost vyexportuje hru do úložiště zařízení umístěného na adrese /storage/android/data/antdroid.cfbcoach/data/documents/ . Tento uložený soubor lze poté přenést do jiného zařízení a použít jej pomocí funkce Import na domovské obrazovce.


# Player Position Info

Když tým zdědíte poprvé, budete mít na soupisce celkem až 70 hráčů. Všechny tyto hráče si můžete prohlédnout na kartě "ROSTER".

## Základní atributy

Každý hráč má obecné vlastnosti, které jsou společné pro každou pozici. Kromě toho je v profilu hráče uveden také jeho domovský stát a hvězdičkový rating v době, kdy byl rekrutován. Walk-On hráči budou zobrazení s "RS":

* **Potential** - Hodnocení potenciálu hráče je skryté. Toto hodnocení je důležité pro rozvoj hráče, ale není to vše pro jeho růst.
* **Personality (Character?)** - Toto hodnocení ukazuje, jak dobře se hráč chová na hřišti i mimo něj. Hráč s nižším osobnostním hodnocením bude mít větší pravděpodobnost, že se dostane do problémů a bude vyloučen nebo dokonce propuštěn z programu. (*Počítá se do týmové chemie pro smazání/zvýšení výhody útoku/obrany.*)
* **Inteligence** - Hodnocení IQ vyjadřuje, jak dobře hráč rozumí fotbalové hře a jak je schopen vést tým během hry. Pomáhá také hráčům činit během hry moudrá rozhodnutí. (*Počítá se do výhodu na domácím hřišti a do týmové chemie.*)
* **Durabilitiy** - Hodnocení odolnosti je náchylnost hráče ke zranění. Při nízkém hodnocení je pravděpodobné, že se hráč v průběhu kariéry zraní. Pokaždé, když se hráč zraní, může jeho hodnocení odolnosti ještě více klesnout. (*Zároveň se počítá do kvality výkonu v průběhu utkání.*)

## Team Chemistry

Týmová chemie není vlastnost hráčů, ale je to celková vlastnost týmu, která ovlivňuje například simulování na hřišti a disciplinární problémy. Týmová chemie je kombinací skóre osobnosti (Character) a fotbalové inteligence. Vyšší hodnocení má za následek lepší sehranost a nižší nefunkčnost šatny.

## Head Coach

Hlavní trenér je vedoucím týmu. To jste vy! Je zodpovědný za stanovení soupisky, výběr správných strategií a nábor týmu.

* **Offensive Ability** - Jedná se o hodnocení trenéra za jeho schopnost volit útočné hry a rozvíjet útok. To má přímý vliv na tvorbu hry ve hře i na rozvoj hráčů.
* **Defensive Ability** - Jedná se o hodnocení trenéra za jeho schopnost řídit obranné hry a rozvíjet útok. To má přímý vliv na určování hry ve hře i na rozvoj hráčů.
* **Talent Progression** - Jedná se o hodnocení trenérovy schopnosti rozvíjet hráče na soupisce a hledat talenty prostřednictvím náboru. Za vyšší hodnocení talentů jsou poskytovány bonusy při náboru (primárně více peněz).
* **Discipline Command** - Jedná se o schopnost trenéra zvládnout členy týmu, kteří mají špatné osobnostní vlastnosti. Čím vyšší je hodnocení disciplíny, tím menší je pravděpodobnost, že tým bude mít suspendované hráče a sankce. Přesto je rozumné nabírat hráče s lepšími osobnostními vlastnostmi.

## Rooster info

* **QB (1/3)** - 1 starter, celkem 3 [info](#quarterback-qb)  
* **RB (2/5)** - 2 starteři, celkem 5 [info](#runningback -rb)   
* **WR (3/8)** - 3 starteři, celkem 8 [info](#wide-receiver-wr)
* **TE (1/3)** -  1 starter, celkem 3 [info](#tight-end-te)  
* **OL (5/11)** - 5 starteru, celkem 11 [info](#offensive-line-ol)
* **K (1/2)** -  1 starter, celkem 2 [info](#kicker-k)  
* **DL (4/10)** - 4 starteři, celkem 10 [info](#defensive-line-dl)
* **LB (3/7)** - 3 starteři, celkem 7 [info](#linebackers-lb)
* **CB (3/7)** - 3 starteři, celkem 7 [info](#cornerback-cb)
* **S (2/5)** - 2 starteři, celkem 5 [info](#safety-s)

## Quarterback (QB)
Rozehrávač je vůdcem útoku. Jeho inteligence je klíčová pro přijímání chytrých rozhodnutí pro tým. Je také zodpovědný za přihrávky svým spoluhráčům a za vybíhání ze situací a občas i za krátký zisk.
* **Pass Strength** - jak daleko může QB házet. Vyšší síla znamená, že se chytá na více yardů.
* **Pass Accuracy** - jak přesný je QB. Vyšší přesnost znamená větší šanci, že bude hod zkompletován, a také nižší šanci na zachycení obráncem.
* **Evasion** - jak je QB vyhýbavý. Větší vyhýbavost znamená méně sacků.
* **Speed** - mobilní rozehrávači jsou schopni se vymanit z rozehrávky a v option hrách běžet s míčem.

## Runningback (RB)
Úkolem runningbeka je běhat s míčem a získávat yardy po zemi. V určitých situacích může občas vyběhnout pro krátkou přihrávku.
* **Rush Power** - jak silný je RB. Vyšší síla znamená, že je méně pravděpodobné, že bude zastaven na LOS.
* **Rush Speed** - jak rychlý je RB. Vyšší rychlost znamená, že běh má více yardů.
* **Evasion** - jak je RB agilní. Vyšší vyhýbavost znamená vyšší šanci na uvolnění pro větší běhy.
* **Catching** - jak dobře umí RB chytat míč. Používá se při krátkých přihrávkách a screenech.

## Wide Receiver (WR)
Úkolem WR je běhat routy a chytat přihrávky vpřed ve hřišti.
* **Catching** - jak dobře WR zachytává míč. Lepší chytání znamená vyšší šanci na zkompletování přihrávky a méně dropů.
* **Speed** - jak rychlý je WR. Vyšší rychlost znamená, že po zachycení udělá více yardů (popř. běhá pro hluboké pasy).
* **Evasion** - jak je WR hbitý a obratný. Vyšší vyhýbavost znamená vyšší šanci na uvolnění pro velké zisky a dlouhé touchdowny.
* **Jumping** - jak vysoko/rychle dokáže WR vyskočit, aby mohl hrát s míčem.

## Tight End (TE)
Tight End je zodpovědný za blokování při běhu a případnou čtvrtou možnost pro přihrávku.
* **Catching** - jak dobře TE chytá míč. Lepší chytání znamená vyšší šanci na zachycení přihrávky a méně dropů.
* **Speed** - jak rychlý je TE. Vyšší rychlost znamená, že po zachycení může udělat více yardů, resp. zachytává delšé přihrávky.
* **Evasion** - jak rychlý je TE. Vyšší rychlost znamená, že po zachycení může udělat více yardů.
* **Run Block** - jak dobře umí TE blokovat při běhových akcích.

## Offensive Line (OL)
Ofenzivní lajna je zodpovědná za ochranu QB a blokování pro RB, když se hraje běhová akce.
* **Strength** - nejdůležitější hodnocení, jak silná je OL.
* **Run Block** - jak dobře umí OL blokovat útoky.
* **Pass Block** - jak dobře umí OL blokovat přihrávky.
* **Vision** - povědomí o okolí má vliv na to, jak dobře spolu OL spolupracují.

## Kicker (k)
Kicker je zodpovědný za kopání extra bodů a gólů.
* **Kick Strength** - jak silná je noha kopajícího. Větší síla znamená delší kopy.
* **Kick Accuracy** - jak přesný je kicker, vyšší znamená, že kopy jsou s vyšší pravděpodobností úspěšné.
* **Form** - asi něco jako šikovnost, důležité pro punty a onside kicky. (asi???)
* **Pressure** - jak dobře zvládá kopající hráč tlak v situacích, jako je 2min drill a prodloužení.

## Defensive Line (DL)
Defenzivní lajna je zodpovědná za tlak na QB soupeře a zastavování RB.
* **Strength** - nejdůležitější hodnocení, jak silná je DL proti soupeřovým OL.
* **Run Stop** - jak dobře se DL dokáže vypořádat se soupeřovými RB, aniž by jim dovolili mnoho yardů.
* **Pass Pressure** - jak velký tlak vyvíjejí na soupeřova QB, vyšší tlak znamená, že QB bude mít větší potíže s přesnými přihrávkami.
* **Tackling** - jak dobře je schopný hráč skládat RB/QB.

## Linebackers (LB)
Linebackeři jsou jedním z lídrů obrany. Jejich úkolem je také bránit receivery i útočníky a pokrývat prostor mezi LOS a secondary. Při přihrávkách může jeden nebo dva z nich krýt tight endy, kteří běží pasovou routu.
* **Coverage** - jak dobře umí LB pokrýt pasy. Vyšší znamená menší pravděpodobnost, že TE zachytí přihrávku.
* **Run Stop** - jak dobře se LB dokáže vypořádat s RB soupeře, aniž by mu dovolil mnoho yardů.
* **Tackling** - jak dobře se LB dokáže skládat soupeřovy RB, QB a TE.
* **Speed** - jak rychlý je LB, ovlivňuje to, jak dokáže držet krok s TE při pasové hře.

## Cornerback (CB)
CB pokrývají WR při pasové hře. Nejlepší CB bude vždy nasazen na nejlepšího WR.
* **Coverage** - jak dobře umí CB pokrýt receivera. Vyšší hodnota znamená menší pravděpodobnost, že WR zachytí přihrávku.
* **Speed** - jak rychlý je CB, ovlivňuje to, jak dokáže držet krok s WR.
* **Tackling** - jak dobrý je CB ve skládání, vyšší znamená, že je pravděpodobnější, že se mu podaří složit WR dříve, než se mu vymaní.
* **Jumping** - jak vysoko a rychle dokáže CB vyskočit a míč srazit nebo zachytit před WR.

## Safety (S)
Dalšími lídry v obraně jsou safeties. Jejich úkolem je zajistit, aby se přes ně v obraně nic nedostalo. Budou pokrývat hráče při přihrávkách a dělat velké zákroky proti útočníkům. Příležitostně mohou zasahovat proti QB.
* **Coverage** - jak dobře umí pokrýt přihrávku a jak dobře umí chytat interceptions.
* **Speed** - jak rychlý je safety, ovlivňuje to, jak dokáže držet krok s WR.
* **Tackling** - jak dobrý je safety ve skládání, vyšší znamená, že je pravděpodobnější, že se vypořádá s WR dříve, než se dostane pryč.
* **Run Stop** - jak dobře safety čte rozehrávku a pokrývá RB.


# Player & Coach Progression

V průběhu sezóny je několik postupových bodů.

## Zlepšení uprostřed sezóny
K prvnímu významnému dochází po 6. týdnu. Jedná se o polovinu sezóny a u hráčů, kteří dosud v sezóně odehráli zápasy, dojde v tomto okamžiku k úpravě hodnocení na základě jejich zkušeností, potenciálu a náhodného faktoru. Na stránce týmu se zobrazí celková změna hodnocení zobrazená po 6. týdnu.

## Změny prestiže
Další bod změn nastane ihned po skončení sezóny. V tomto bodě se spočítá skóre týmu za sezónu a prestiž se přidá nebo ubere v závislosti na několika faktorech, včetně výkonu podle plánu, umístění, získaných trofejí, disciplíny a síly vítězství.

Pokud jsou povoleny televizní smlouvy, TV stanice rozdělí body prestiže, pokud je s konferencí uzavřena smlouva.

## Zlepšení trenérů
Po vypršení trenérské smlouvy je pro trenéry připraven další postupový bod. V tomto bodě trenéři uvidí změny smluv, propuštění, odchody do důchodu, nástupy a celkový postup jednotlivých trenérů. Jejich hodnocení se mění na základě výkonnosti týmu v dané sezóně:
 * Bodové hodnocení talentu se mění na základě výkonu týmu (změna prestiže - bez započtení bodů za disciplínu).
 * Disciplinární skóre se mění na základě disciplinárních opatření v průběhu sezóny.
 * Hodnocení útoku a obrany se mění na základě příslušných výkonů oproti průměru ligy s koeficientem založeným na tom, jak je tým talentovaný oproti průměru. Pokud je váš tým velmi talentovaný, očekává se od vás lepší výkon než průměr.

## Konec sezóny
Při ukončení studia hráči absolvují, přestoupí, jsou propuštěni ze školy a postupují. Změny jejich hodnocení jsou založeny na jejich hodnocení talentu, herních zkušenostech a některých náhodných faktorech. Někteří hráči se mohou zhoršit v závislosti na zranění a dalších faktorech, ale u většiny dojde k nárůstu hodnocení.

Přestupy budou založeny na dostupnosti herního času pro každého hráče v jeho týmu. Někteří hráči budou chtít nastupovat pravidelně a rozhodnou se pro přestup. Někteří přestupující budou absolventy, kteří nebudou muset vynechat ročník, ale většina z nich jsou přestupující vysokoškoláci a budou muset vynechat jednu sezónu v novém týmu.

A konečně další skupina elitních hráčů bude chtít předčasně ohlásit účast v profesionálním fotbalovém draftu. Tito hráči odejdou již v druhého (RS) nebo třetího ročníku. Mock Draft si můžete prohlédnout na poslední obrazovce před náborem.


# Offense & Defense Strategies

Jako trenér si můžete zvolit strategii, kterou váš tým v průběhu sezóny použije v útoku a obraně. Ve výchozím nastavení jsou obě strategie nastaveny na vyvážené útoky a obrany, což znamená, že v obou případech nejsou žádné bonusy ani tresty. Strategii však můžete změnit na některou z následujících:

## Ofenzivní schéma
### Pro-Style
 * Vybalancovaná ofenzíva
 * 50/50 pasy a běhy
 * Žádné silné ani slabé stránky

### Smash Mouth
 * Silně běhová ofenzíva
 * 60/40 běhy vůči pasům
 * Bonusy za blokování, kratší zisky, více času mimo hrací dobu
 
### West Coast
 * Upřednostnění přihrávek (kratších), před běhy
 * 60/40 pasů vůči běhům
 * Bonusy za blokování, kratší zisky, více času mimo hrací dobu
 
### Spread
 * Silně pasová ofenzíva
 * 72 % pasů
 * Horší blokování (záporný bonus), potenciál na big plays
 
### Read Option
 * Upřednostnění běhu vůči pasům (vyžaduje mobilního QB)
 * 55 % běhů
 * Horší blokování (záporný bonus), potenciál na big plays
 
### Run-Pass Option
 * Upřednostnění pasů vůči běhům (vyžaduje mobilního QB)
 * 55 % pasů
 * Horší blokování (záporný bonus), potenciál na big plays

### Využití schémat proti obraně:
 * **4-6 Bear** - Spread, West Coast nebo RPO
 * **Cover 0** - Spread (dobrý pasový QB), Smash Mouth (špatný nebo mobilní QB), prý je možné využít i West Coast (jen s dobrými WR)
 * **4-3** - Spread, Optiony, Smash Mouth (podle toho co vyhovuje vlastním přednostem, nebo soupeřovým slabinám)
 * **Cover 2** - Smash mouth nebo Read-Option; na Cover 2 je možné nasadit i Pro-Style (pouze se silnými RB)
 * **Cover 3** - Smash mouth nebo Read-Option;
 
## Defenzivní schéma
### 4-3 Man
 * Vyrovnaná základní defenzíva
 * Bez bonusů nebo penalizací
 
### 4-6 Bear
 * Obranný systém s důrazem na omezení běhů
 * Povoluje méně běhových yardů, ale secondary je náchylná k velkým ztrátám.
 
### Cover 0
 * Osobní obrana (man-to-man)
 * Žádný obránce není v hluboké zóně
 
### Cover 2
 * Zónová obrana nastavená tak, aby zastavovala přihrávky
 * Zlepšuje pokrytí přihrávek, ale může umožnit větší zisky z běhů.
 
### Cover 3
 * Zónová obrana postavená pro zastavení dlouhých pasových her
 * Výrazně zlepšuje pokrytí přihrávek a zastavuje dlouhé pasy.
 * Obrana může povolit dlouhé běhové hry.

### Využití schémat proti útoku:
 * **Smash mouth** - 4-6 Bear (zastavování běhu)
 * **Read Option** - 4-3 nebo 4-6
 * **Spread** - Cover 3 (zastavení dlouhých her)
 * **Run-Pass Option, West Coast** - Cover 2 (proti WC by to bylo 3-4)
 * **Pro-Style** - 4-3 nebo Cover 0, ale lze použít cokoli co vyhovuje vlastním přednostem, nebo soupeřovým slabinám

 
# Recruiting
Nábor je klíčovou součástí každého vysokoškolského programu. Nábor v této hře je velmi jednoduchý a rychlý, aby i samotná hra zůstala jednoduchá a rychlá.

K dispozici je seznam rekrutů, které vám poskytne váš skautský tým. Máte k dispozici peněžní rozpočet, který můžete utratit za nábor hráčů (výše rozpočtu je dána kvalitou trenérů /talent/ a počtem chybějících hráčů). Hráči vyšších úrovní vyžadují větší rozpočet na nábor. Každý hráč má různé atributy s poskytovanými skautskými známkami 1-5 hvězdiček. Potenciál je skrytý.

Každý atribut je pro váš tým důležitý, jak bylo zmíněno v předchozích částech. Využívejte prosím svůj rozpočet moudře a nezapomeňte doplnit svou sestavu kvalitními hráči, protože na náhradnících a celkové týmové chemii záleží!


# Game Editor
V poslední verzi je možné editovat celou hru pomocí vestavného editoru. Nyní je možné měnit názvy konferencí, týmů, prestiž, soupeře (schedule nebo rivality???), jména trenérů.


# Zdroje
https://www.reddit.com/r/FootballCoach/comments/9h0fyy/which_playbook/
https://www.reddit.com/r/FootballCoach/comments/8izbkj/direct_counters_play_calling_simplified/
https://www.reddit.com/r/FootballCoach/comments/8ixe2a/play_calling_direct_counters/?utm_source=reddit-android
https://www.reddit.com/r/FootballCoach/comments/cech12/college_football_coach_2_play_calling/
https://www.reddit.com/r/FootballCoach/

https://www.antdroid.dev/p/game-manual.html