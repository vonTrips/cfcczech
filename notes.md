# Poznámky k hraní

## Atributy
Inteligence
 - důležitá pro výhodu domácího hřiště (buď ji doma zvýším nebo venku smažu)
 - počítá se i do týmové chemie (=> zvýšení/smazání výhody útoku/obrany)
 
Charakter
 - počítá se i do týmové chemie (=> zvýšení/smazání výhody útoku/obrany)
 
Durability
 - čím vyšší, tím nižší je riziko zranění
 - čím vyšší, tím déle si hráč udrží vysokou výkonnost v zápase 

Koordinátor
 - talent přidává peníze do náboru a zvyšuje pravděpodobnost zlepšení hráče
 - úroveň znalosti přidává hodnocení (advantage)
 
Rychlost WR, RB, CB
 - důležité pro returny
 - případně pro coverage odkopávajícího týmu

## Strategie 
### Counter strategie
Co nasadit proti útoku:

0. **Pro style** < 4-3 Man
1. **Smash Mouth** < 4-6 Bear
2. **West Coast** < Cover 0 / Cover 2 (podle kvality obrany)
3. **Spread** < Cover 3
4. **Read Option** < 4-6 Bear
5. **Run Pass Option** < Cover 2

Co nasadit proti obraně:

0. **4-3 Man** < West Coast / RPI (?)
1. **4-6 Bear** < Spread / West Coast
2. **Cover 0** < Spread (a dal nevim)
3. **Cover 2** < Pro Style / Read Option
4. **Cover 3** < Smash Mouth / Read Option

### ÚTOK
WestCoast / Run-Pass Option
 - vyžaduje solidní RB
 - QB by měl mít dobrou Accurancy a Speed, vhodné i Evasion

Penalizace za rozdílný playbook vůči HC a OC/DC je maximálně -2 při hodnotách převyšujících 100
 
### OBRANA
4-3 / Cover 2
 - vhodnější může být Cover 2 kvůli bonusům (de facto se hodí proti všem útokům)
 - 4-3 může být jako obecná strategire
 - Bear (4-6) proti tvrdému běhu

## Rekrutování
 - Vždy je třeba maximálně natankovat obranu, ofenziva se dá zamaskovat
 - Základní rada dle fóra však zní že základem jsou QB, RB a S
 - Před rekrutováním je vhodné vyhodit co nejvíce nekvalitních hráčů, tím se dá získat docela dost peněz navíc (každý vyhozený hráč je +30 $)
 - není třeba rekrutovat pouze TOP hráče, ideální je do počtu rekrutovat hráče s 5* charakterem a inteligencí (obojí je důležité pro zápasy a počítá se ze všech hráčů, myslím)